﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Laba2NYSS
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            fromPC_Click(null, null);
            if (gridList.Count == 0)
            {
                MessageBox.Show("Чтобы загрузить базу из интернета нажмите \"Скачать и вывести\"");
            }
        }
        public int page = 1;
        public List<Grid> gridListToShow = new List<Grid>();
        public List<Grid> gridList = new List<Grid>();
        public List<bool> gridChanged = new List<bool>();
        public List<Grid> gridListNonUpdateble = new List<Grid>();
        public string desktop = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
        public Uri uri = new Uri("https://bdu.fstec.ru/files/documents/thrlist.xlsx");
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            gridList.Clear();
            List<object> excelData = new List<object>();
            WebClient client = new WebClient();
            byte[] bin = client.DownloadData(uri);
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            using (MemoryStream stream = new MemoryStream(bin))
            {
                using (ExcelPackage excelPackage = new ExcelPackage(stream))
                {
                    //loop all worksheets
                    foreach (ExcelWorksheet worksheet in excelPackage.Workbook.Worksheets)
                    {
                        //loop all rows
                        for (int i = worksheet.Dimension.Start.Row; i <= worksheet.Dimension.End.Row; i++)
                        {
                            //loop all columns in a row
                            for (int j = worksheet.Dimension.Start.Column; j <= worksheet.Dimension.End.Column; j++)
                            {
                                //add the cell data to the List
                                if (worksheet.Cells[i, j].Value != null)
                                {
                                    excelData.Add(worksheet.Cells[i, j].Value);
                                }
                            }
                        }
                    }
                }
            }
            for (int i = 0; i < excelData.Count; i++)
            {
                if (i < 3)
                {
                    Grid.firstLine.Add((string)excelData[i]);
                }
                else if (i < 13)
                {
                    Grid.secondLine.Add((string)excelData[i]);
                }
                else
                {
                    switch (i % 10)
                    {
                        case 3:
                            gridList.Add(new Grid());
                            gridList.Last().Id = Convert.ToInt32(excelData[i]);
                            break;
                        case 4:
                            gridList.Last().Name = (string)excelData[i];
                            break;
                        case 5:
                            gridList.Last().Description = (string)excelData[i];
                            break;
                        case 6:
                            gridList.Last().Source = (string)excelData[i];
                            break;
                        case 7:
                            gridList.Last().Target = (string)excelData[i];
                            break;
                        case 8:
                            gridList.Last().Confid = Convert.ToInt32(excelData[i]);
                            break;
                        case 9:
                            gridList.Last().Integrity = Convert.ToInt32(excelData[i]);
                            break;
                        case 0:
                            gridList.Last().Access = Convert.ToInt32(excelData[i]);
                            break;
                        case 1:
                            //gridList.Last().InDate = DateTime.FromOADate((double)excelData[i]);
                            break;
                        case 2:
                            //gridList.Last().LastChangeDate = DateTime.FromOADate((double)excelData[i]);
                            break;
                        default:
                            break;
                    }
                }
            }
            SetPageWithList(page);
            dataGrid.ItemsSource = null;
            dataGrid.ItemsSource = gridListToShow;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (gridList.Count != 0)
            {
                using (BinaryWriter bW = new BinaryWriter(File.Create(desktop + "\\Ybi.txt")))
                {
                    foreach (var item in gridList)
                    {
                        bW.Write(item.Id);
                        bW.Write(item.Name);
                        bW.Write(item.Description);
                        bW.Write(item.Source);
                        bW.Write(item.Target);
                        bW.Write(item.Confid);
                        bW.Write(item.Integrity);
                        bW.Write(item.Access);
                        //bW.Write(item.InDate.ToString());
                        //bW.Write(item.LastChangeDate.ToString());
                    }
                    MessageBox.Show("Локальная база сохранена в файл на рабочем столе");
                }
            }
            else
            {
                MessageBox.Show("Локальной базы нет");
            }

        }

        private void delete_Click(object sender, RoutedEventArgs e)
        {
            gridList.Clear();
            page = 1;
            SetPageWithList(page);
            dataGrid.ItemsSource = null;
            dataGrid.ItemsSource = gridListToShow;
            MessageBox.Show("Локальная база удалена");
        }

        private void fromPC_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (File.Exists(desktop + "\\Ybi.txt"))
                {
                    gridList.Clear();
                    using (BinaryReader bR = new BinaryReader(File.Open(desktop + "\\Ybi.txt", FileMode.Open)))
                    {
                        while (bR.BaseStream.Position != bR.BaseStream.Length)
                        {
                            Grid grd = new Grid();
                            grd.Id = bR.ReadInt32();
                            grd.Name = bR.ReadString();
                            grd.Description = bR.ReadString();
                            grd.Source = bR.ReadString();
                            grd.Target = bR.ReadString();
                            grd.Confid = bR.ReadInt32();
                            grd.Integrity = bR.ReadInt32();
                            grd.Access = bR.ReadInt32();
                            //grd.InDate = Convert.ToDateTime(bR.ReadString());
                            //grd.LastChangeDate = Convert.ToDateTime(bR.ReadString());
                            gridList.Add(grd);
                        }
                    }
                    SetPageWithList(page);
                    dataGrid.ItemsSource = null;
                    dataGrid.ItemsSource = gridListToShow;
                    MessageBox.Show("База с рабочего стола загружена в приложение");
                }
                else
                {
                    MessageBox.Show("Файла не существует");
                }
            }
            catch
            {
                MessageBox.Show("Данные в файле повреждены");
            }
        }

        private void update_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                gridListNonUpdateble.Clear();
                gridChanged.Clear();
                foreach (var item in gridList)
                {
                    gridListNonUpdateble.Add((Grid)item.Clone());
                }
                for (int i = 0; i < gridListNonUpdateble.Count; i++)
                {
                    gridChanged.Add(false);
                }
                List<object> excelData = new List<object>();
                WebClient client = new WebClient();
                byte[] bin = client.DownloadData(uri);
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

                using (MemoryStream stream = new MemoryStream(bin))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(stream))
                    {
                        //loop all worksheets
                        foreach (ExcelWorksheet worksheet in excelPackage.Workbook.Worksheets)
                        {
                            //loop all rows
                            for (int i = worksheet.Dimension.Start.Row; i <= worksheet.Dimension.End.Row; i++)
                            {
                                //loop all columns in a row
                                for (int j = worksheet.Dimension.Start.Column; j <= worksheet.Dimension.End.Column; j++)
                                {
                                    //add the cell data to the List
                                    if (worksheet.Cells[i, j].Value != null)
                                    {
                                        excelData.Add(worksheet.Cells[i, j].Value);
                                    }
                                }
                            }
                        }
                    }
                }
                int n;
                for (int i = 0; i < excelData.Count; i++)
                {
                    if (i < 3)
                    {
                        if (Grid.firstLine.Count > i)
                        {
                            if (Grid.firstLine[i] != (string)excelData[i])
                            {
                                Grid.firstLine[i] = ((string)excelData[i]);
                            }
                        }
                        else
                        {
                            Grid.firstLine.Add((string)excelData[i]);
                        }
                    }
                    else if (i < 13)
                    {
                        if (Grid.secondLine.Count > i - 3)
                        {
                            if (Grid.secondLine[i - 3] != (string)excelData[i])
                            {
                                Grid.secondLine[i - 3] = ((string)excelData[i]);
                            }
                        }
                        else
                        {
                            Grid.secondLine.Add((string)excelData[i]);
                        }
                    }
                    else
                    {
                        switch (i % 10)
                        {
                            case 3:
                                n = i / 10 - 1;
                                if (gridList[n].Id != Convert.ToInt32(excelData[i]))
                                {
                                    gridList[n].Id = Convert.ToInt32(excelData[i]);
                                    gridChanged[n] = true;
                                }
                                break;
                            case 4:
                                n = i / 10 - 1;
                                if (gridList[n].Name != (string)excelData[i])
                                {
                                    gridList[n].Name = (string)excelData[i];
                                    gridChanged[n] = true;
                                }
                                break;
                            case 5:
                                n = i / 10 - 1;
                                if (gridList[n].Description != (string)excelData[i])
                                {
                                    gridList[n].Description = (string)excelData[i];
                                    gridChanged[n] = true;
                                }
                                break;
                            case 6:
                                n = i / 10 - 1;
                                if (gridList[n].Source != (string)excelData[i])
                                {
                                    gridList[n].Source = (string)excelData[i];
                                    gridChanged[n] = true;
                                }
                                break;
                            case 7:
                                n = i / 10 - 1;
                                if (gridList[n].Target != (string)excelData[i])
                                {
                                    gridList[n].Target = (string)excelData[i];
                                    gridChanged[n] = true;
                                }
                                break;
                            case 8:
                                n = i / 10 - 1;
                                if (gridList[n].Confid != Convert.ToInt32(excelData[i]))
                                {
                                    gridList[n].Confid = Convert.ToInt32(excelData[i]);
                                    gridChanged[n] = true;
                                }
                                break;
                            case 9:
                                n = i / 10 - 1;
                                if (gridList[n].Integrity != Convert.ToInt32(excelData[i]))
                                {
                                    gridList[n].Integrity = Convert.ToInt32(excelData[i]);
                                    gridChanged[n] = true;
                                }
                                break;
                            case 0:
                                n = i / 10 - 2;
                                if (gridList[n].Access != Convert.ToInt32(excelData[i]))
                                {
                                    gridList[n].Access = Convert.ToInt32(excelData[i]);
                                    gridChanged[n] = true;
                                }
                                break;
                            case 1:
                                //n = i / 10 - 2;
                                //if (gridList[n].InDate != DateTime.FromOADate((double)excelData[i]))
                                //{
                                //    gridList[n].InDate = DateTime.FromOADate((double)excelData[i]);
                                //    gridChanged[n] = true;
                                //}
                                break;
                            case 2:
                                //n = i / 10 - 2;
                                //if (gridList[n].LastChangeDate != DateTime.FromOADate((double)excelData[i]))
                                //{
                                //    gridList[n].LastChangeDate = DateTime.FromOADate((double)excelData[i]);
                                //    gridChanged[n] = true;
                                //}
                                break;
                            default:
                                break;
                        }
                    }
                }
                int count = 0;
                foreach (var item in gridChanged)
                {
                    if (item == true)
                    {
                        count++;
                    }
                }
                MessageBox.Show("Обновление прошло успешно\nЗаписей изменено: " + count);

                string show = "";
                for (int i = 0; i < gridList.Count; i++)
                {
                    bool flag = false;
                    if (gridList[i].Name != gridListNonUpdateble[i].Name)
                    {
                        if (flag == false)
                        {
                            show += "Id:" + gridList[i].Id + "\n";
                            flag = true;
                        }
                        show += gridListNonUpdateble[i].Name + " — " + gridList[i].Name + "\n";
                    }
                    if (gridList[i].Description != gridListNonUpdateble[i].Description)
                    {
                        if (flag == false)
                        {
                            show += "Id:" + gridList[i].Id + "\n";
                            flag = true;
                        }
                        show += gridListNonUpdateble[i].Description + " — " + gridList[i].Description + "\n";
                    }
                    if (gridList[i].Source != gridListNonUpdateble[i].Source)
                    {
                        if (flag == false)
                        {
                            show += "Id:" + gridList[i].Id + "\n";
                            flag = true;
                        }
                        show += gridListNonUpdateble[i].Source + " — " + gridList[i].Source + "\n";
                    }
                    if (gridList[i].Target != gridListNonUpdateble[i].Target)
                    {
                        if (flag == false)
                        {
                            show += "Id:" + gridList[i].Id + "\n";
                            flag = true;
                        }
                        show += gridListNonUpdateble[i].Target + " — " + gridList[i].Target + "\n";
                    }
                    if (gridList[i].Confid != gridListNonUpdateble[i].Confid)
                    {
                        if (flag == false)
                        {
                            show += "Id:" + gridList[i].Id + "\n";
                            flag = true;
                        }
                        show += gridListNonUpdateble[i].Confid + " — " + gridList[i].Confid + "\n";
                    }
                    if (gridList[i].Integrity != gridListNonUpdateble[i].Integrity)
                    {
                        if (flag == false)
                        {
                            show += "Id:" + gridList[i].Id + "\n";
                            flag = true;
                        }
                        show += gridListNonUpdateble[i].Integrity + " — " + gridList[i].Integrity + "\n";
                    }
                    if (gridList[i].Access != gridListNonUpdateble[i].Access)
                    {
                        if (flag == false)
                        {
                            show += "Id:" + gridList[i].Id + "\n";
                            flag = true;
                        }
                        show += gridListNonUpdateble[i].Access + " — " + gridList[i].Access + "\n";
                    }
                    //if (gridList[i].InDate != gridListNonUpdateble[i].InDate)
                    //{
                    //    if (flag == false)
                    //    {
                    //        show += "Id:" + gridList[i].Id + "\n";
                    //        flag = true;
                    //    }
                    //    show += gridListNonUpdateble[i].InDate + " — " + gridList[i].InDate + "\n";
                    //}
                    //if (gridList[i].LastChangeDate != gridListNonUpdateble[i].LastChangeDate)
                    //{
                    //    if (flag == false)
                    //    {
                    //        show += "Id:" + gridList[i].Id + "\n";
                    //        flag = true;
                    //    }
                    //    show += gridListNonUpdateble[i].LastChangeDate + " — " + gridList[i].LastChangeDate + "\n";
                    //}
                }
                if (show != "")
                {
                    MessageBox.Show(show);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка обновления\n" + ex.Message);
            }
            finally
            {
                SetPageWithList(page);
                dataGrid.ItemsSource = null;
                dataGrid.ItemsSource = gridListToShow;
            }
        }

        private void shortForm_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<string, string> shortForm = new Dictionary<string, string>();
            foreach (var item in gridList)
            {
                string val = "";
                if (item.Id < 10)
                {
                    val = "УБИ.00" + Convert.ToString(item.Id);
                }
                else if (item.Id / 10 < 10)
                {
                    val = "УБИ.0" + Convert.ToString(item.Id);
                }
                else if (item.Id / 100 < 10)
                {
                    val = "УБИ." + Convert.ToString(item.Id);
                }
                else
                {
                    MessageBox.Show("????????");
                }
                shortForm.Add(val, item.Name);
            }
            shortDataGrid.ItemsSource = null;
            shortDataGrid.ItemsSource = shortForm;
        }

        private void IdButton_Click(object sender, RoutedEventArgs e)
        {
            int numb;
            if (Int32.TryParse(idChoose.Text, out numb))
            {
                if (gridList.Count > numb - 1)
                {
                    string info = "Идентификатор: " + gridList[numb - 1].Id +
                        "\nНаименование: " + gridList[numb - 1].Name +
                        "\nОписание: " + gridList[numb - 1].Description +
                        "\nИсточник: " + gridList[numb - 1].Source +
                        "\nОбъект воздействия: " + gridList[numb - 1].Target +
                        "\nНарушение конфиденциальности: " + gridList[numb - 1].Confid +
                        "\nНарушение целостности: " + gridList[numb - 1].Integrity +
                        "\nНарушение доступности: " + gridList[numb - 1].Access;
                    MessageBox.Show(info);
                }
                else
                {
                    MessageBox.Show("Записи с таким id не существует");
                }
            }
            else
            {
                MessageBox.Show("Нужно ввести число");
            }
        }
        public void SetPageWithList(int pg)
        {
            gridListToShow.Clear();
            for (int firstEl = pg * 20 - 20; firstEl < pg * 20; firstEl++)
            {
                if (gridList.Count > firstEl && firstEl >= 0)
                {
                    gridListToShow.Add(gridList[firstEl]);
                }
            }
        }

        private void leftPage_Click(object sender, RoutedEventArgs e)
        {
            if (page != 1)
            {
                page--;
                SetPageWithList(page);
                dataGrid.ItemsSource = null;
                dataGrid.ItemsSource = gridListToShow;
            }
        }

        private void rightPage_Click(object sender, RoutedEventArgs e)
        {
            if ((page + 1) * 20 - 19 < gridList.Count)
            {
                page++;
                SetPageWithList(page);
                dataGrid.ItemsSource = null;
                dataGrid.ItemsSource = gridListToShow;
            }
        }
    }


    [Serializable]
    public class Grid : ICloneable
    {
        [NonSerialized]
        public static List<string> firstLine = new List<string>();
        [NonSerialized]
        public static List<string> secondLine = new List<string>();
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public string Target { get; set; }
        public int Confid { get; set; }
        public int Integrity { get; set; }
        public int Access { get; set; }
        //public DateTime InDate { get; set; }
        //public DateTime LastChangeDate { get; set; }
        public object Clone()
        {
            return new Grid {
                Id = this.Id,
                Name = this.Name,
                Description = this.Description,
                Source = this.Source,
                Target = this.Target,
                Confid = this.Confid,
                Integrity = this.Integrity,
                Access = this.Access,
                //InDate = this.InDate,
                //LastChangeDate = this.LastChangeDate
            };
        }
    }
}
